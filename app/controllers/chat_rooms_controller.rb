class ChatRoomsController < ApplicationController
  NOT_FOUND_MESSAGE = 'Not found'

  def index
    render json: chat_rooms
  end

  def create
    chat_room = ChatRoom.new(chat_room_params)
    if chat_room.save
      render json: chat_room
    else
      render json: {errors: chat_room.errors.full_messages}, status: 422
    end
  end

  def show
    if chat_room
      render json: chat_room, include: [:messages]
    else
      render json: {errors: NOT_FOUND_MESSAGE}, status: 404
    end
  end

  private

  def chat_room_params
    params.require(:chat_room).permit(:name)
  end

  def chat_rooms
    @chat_rooms ||= ChatRoom.all
  end

  def chat_room
    @chat_room ||= begin
                     ChatRoom.find(params[:id])
                   rescue
                     nil
                   end
  end
end
